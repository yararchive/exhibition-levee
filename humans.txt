/* humanstxt.org */

/* TEAM */
  project director: Andrey Zvorygin
  e-mail: umount.dev.brain@ya.ru
  twitter: umount_devbrain
  vk: http://vk.com/umount.dev.brain
  location: Yaroslavl, Russia

/* SITE */                          
  Last update: 2014/05/22 
  Standards: HTML5, CSS3
  Components: jQuery 2, Zurb Foundation 5, Modernizr, ScrollMagic
  Software: Sublime Text 3, Git